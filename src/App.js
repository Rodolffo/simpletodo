import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Alert, FormGroup, FormFeedback } from 'reactstrap';
import DatePicker from 'react-datepicker';
import TodoItem from './TodoItem';

import 'bootstrap/dist/css/bootstrap.css';
import 'react-datepicker/dist/react-datepicker.css';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    let storedItems = JSON.parse(localStorage.getItem("todoItems"));
    this.state = {
      todoDate: null,
      todoName: "",
      todoItems: (storedItems === null) ? [] : storedItems,
      lastDeleted: null,
      recentDelete: false,
      invalidName: false
    };
  }

  componentDidUpdate(prevProps, prevState) {
    localStorage.setItem("todoItems", JSON.stringify(this.state.todoItems));
  }

  undoLastDelete = () => {
    let newArr = [];
    this.state.todoItems.forEach( (item) => {
      if(item.order >= this.state.lastDeleted.order)
        item.order += 1;
      newArr.push(item);
    });
    newArr.push(this.state.lastDeleted);
    newArr.sort(this.sortByOrder);
    this.setState({ todoItems: newArr, recentDelete: false });
  }
  sortByOrder(a, b) {
    if (a.order > b.order) return 1;
    if (a.order < b.order) return -1;
    return 0;
  }

  checkItem = (order) => {
    let newArr = [];
    this.state.todoItems.forEach((item) => {
      if(item.order === order)
        item.checked = !item.checked;
      newArr.push(item);
    });
    newArr.sort(this.sortByOrder);
    this.setState({ todoItems: newArr });
  }

  moveItemUp = (order) => {
    let newArr = [];
    this.state.todoItems.forEach((item) => {
      if(item.order === order)
        item.order -= 1;
      else if(item.order === order - 1)
        item.order += 1;
      newArr.push(item);
    });
    newArr.sort(this.sortByOrder);
    this.setState({ todoItems: newArr });
  }

  moveItemDown = (order) => {
    let newArr = [];
    this.state.todoItems.forEach((item) => {
      if(item.order === order)
        item.order += 1;
      else if(item.order === order + 1)
        item.order -= 1;
      newArr.push(item);
    });
    newArr.sort(this.sortByOrder);
    this.setState({ todoItems: newArr });
  }

  addTodoItem = () => {
    if(this.state.todoName.trim() === "") {
      this.setState({ invalidName: true });
      setTimeout(() => this.setState({ invalidName: false }), 3000);
    }
    else {
      let newItems = this.state.todoItems;
      let lastOrder = 1;
      this.state.todoItems.forEach((item) => {
        lastOrder = item.order + 1;
      });
      newItems.push({name: this.state.todoName, date: (this.state.todoDate === null) ? "" : this.state.todoDate.format("MM/DD/YYYY"), checked: false, order: lastOrder});
      this.setState({ todoItems: newItems, todoDate: null, todoName: "", invalidName: false });
    }
  }

  todoRemoved = (order) => {
    let newItems = [];
    let deletedItem = null;
    this.state.todoItems.forEach((item) => { 
      if(item.order !== order) {
        if(item.order > order)
          item.order -= 1;
        newItems.push(item);    
      }
      else
        deletedItem = item;
    })
    this.setState({ todoItems: newItems, lastDeleted: deletedItem, recentDelete: true});
    setTimeout(() => this.setState({ recentDelete: false }), 4000);
  }

  render() {
    return (
      <div className="main-container">
        <h1 className="main-head">Simple Todo</h1>
        <Container>
          <Row className="d-flex justify-content-center">
            <Col sm={5}>
              <Input type="text" value={this.state.todoName} placeholder="Todo name" onChange={(e) => this.setState({ todoName: e.target.value })} />
            </Col>
            <Col sm={2}>
              <DatePicker locale="en-GB" placeholderText="Date" className="form-control" isClearable={true} selected={this.state.todoDate} 
              onChange={(date) => { this.setState({ todoDate: date }); }} />
            </Col>
            <Col sm={2}>
              <Button onClick={this.addTodoItem}>Add Todo</Button>
            </Col>
          </Row>
        </Container>
        <div className="item-container">
          {this.state.todoItems.length === 0 ? "The list is empty." : ""}
          {this.state.todoItems.map((item) => {
            return(
              <TodoItem key={item.order} order={item.order} name={item.name} date={item.date} 
              checked={item.checked} onTodoRemoved={this.todoRemoved} checkItem={this.checkItem} 
              last={item.order === this.state.todoItems.length ? true : false} 
              moveItemUp={this.moveItemUp} moveItemDown={this.moveItemDown}/>
          )})}
        </div>
        <Alert color="danger" className="alert-bottom" style={{ display: (this.state.recentDelete) ? "inline" : "none" }}>
          Removed todo. <a onClick={this.undoLastDelete} style={{cursor: "pointer"}}>UNDO</a>
        </Alert>
        <Alert color="danger" className="alert-bottom" style={{ display: (this.state.invalidName) ? "inline" : "none" }}>
          Todo name cannot be empty!
        </Alert>
      </div>
    );
  }
}

export default App;

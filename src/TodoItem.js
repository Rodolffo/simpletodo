import React, { Component } from 'react';
import { Row, Col, Container, Input } from 'reactstrap';
import { FaTimesCircle, FaArrowUp, FaArrowDown } from 'react-icons/fa';

export default class TodoItem extends Component {
    render() {
        return(
            <div className={this.props.checked ? "todo-item checked" : "todo-item"}>
            <Container>
                <Row>
                    <Col className="todo-checkbox" xs={2}>
                        <Input type="checkbox" checked={this.props.checked} onChange={() => this.props.checkItem(this.props.order) }/>
                    </Col>
                    <Col xs={5}>
                        {this.props.name}
                    </Col>
                    <Col xs={2}>
                        {this.props.date}
                    </Col>
                    <Col className="todo-delete" xs={3}>
                        {(this.props.order === 1) ? "" : <FaArrowUp onClick={() => this.props.moveItemUp(this.props.order)} /> }
                        {(this.props.last) ? "" : <FaArrowDown onClick={() => this.props.moveItemDown(this.props.order)} />}
                        <FaTimesCircle onClick={() => this.props.onTodoRemoved(this.props.order)} />
                    </Col>
                </Row>
            </Container>
            </div>
        );
    }
}